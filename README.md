# Backend task journal

## Description
A module for saving and scheduling tasks in the absence of a database.
No any `configuration` required.

1. Only for use in s2m modules.
2. The module is supplied in source and needs to be built.
3. The module can be builded in the following ways:
    - using npm script `npm explore @s2m/journal -- npm i && npm explore @s2m/journal -- npm run build`

## Features
- Dump tasks onto the journal to file
- Restore dumped journal's tasks on startup

## Installation
1. clone project `git clone git@gitlab.com:set2meet/utils/journal.git`
2. run `npm i`
3. run `npm run build`

## Constructor API

### Create journal

#### Typescript
```typescript
import Journal from '@s2m/journal';

const TaskDesc = {
  id: string;
  step: number;
};

const journal = new Journal<TaskDesc>(onTaskEvent);

journal.onTask((task) => {
  console.log(`[journal] task executed: ${task.id}, ${task.step}`)
});

```
### Create journal task

```typescript
import Journal from '@s2m/journal';

const TaskDesc = {
  id: string;
  step: number;
};

const journal = new Journal<TaskDesc>(onTaskEvent);

journal.createTask({
  id: 'xx-yy-id',
  step: 2,
}, 30000);
```

### Remove Journal task

Task can be removed by any field

```typescript
import Journal from '@s2m/journal';

const TaskDesc = {
  id: string;
  step: number;
  action: 'create' | 'remove'
};

const journal = new Journal<TaskDesc>(onTaskEvent);

journal.createTask({
  id: 'xx-yy-ww',
  step: 2,
  action: 'remove',
}, 30000);

journal.createTask({
  id: 'xx-yy-zz',
  step: 2,
  action: 'create',
}, 30000);

// ... some times later

// the next call will delete both the tasks
// created above

journal.removeTask({
  step: 2,
});
```
