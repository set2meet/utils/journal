"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Task {
    constructor(ops) {
        const { desc, date, remove, execute, } = ops;
        this._desc = desc;
        this._onRemove = remove;
        this._onExecute = execute;
        if (typeof date === 'string') {
            // means default date string
            this._date = new Date(date);
        }
        else if (typeof date === 'number') {
            // means ops.date is tiome shift from now
            this._date = new Date(Date.now() + date);
        }
        // schedule task execution event
        this._timeout = setTimeout(() => this._execute(), this._date.getTime() - Date.now());
    }
    get date() {
        return this._date;
    }
    get desc() {
        return this._desc;
    }
    remove() {
        clearTimeout(this._timeout);
        this._onRemove(this);
    }
    _execute() {
        this._onExecute(this);
    }
}
exports.default = Task;
//# sourceMappingURL=task.js.map