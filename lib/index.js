"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const filter_1 = __importDefault(require("lodash/filter"));
const dump_1 = require("./dump");
const task_1 = __importDefault(require("./task"));
const SPLITTER = '\n';
class Journal {
    constructor() {
        this._tasks = [];
        this._queueExecTasks = [];
        this._taskRemoved = (task) => {
            const inx = this._tasks.indexOf(task);
            if (inx > -1) {
                this._tasks.splice(inx, 1);
                this._save();
            }
        };
        this._taskExecuted = (task) => {
            if (this._onTaskExecute) {
                task.remove();
                this._onTaskExecute(task.desc);
            }
            else {
                this._queueExecTasks.push(task);
            }
        };
        this._run();
    }
    onTask(fn) {
        this._onTaskExecute = fn;
        this._queueExecTasks.forEach(this._taskExecuted);
        this._queueExecTasks.length = 0;
    }
    createTask(desc, date) {
        const task = this._taskBuild(desc, date);
        this._save();
        return task;
    }
    removeTask(desc) {
        filter_1.default(this._tasks, desc).forEach((task) => task.remove());
    }
    _taskBuild(desc, date) {
        const task = new task_1.default({
            desc,
            date,
            remove: this._taskRemoved,
            execute: this._taskExecuted,
        });
        this._tasks.push(task);
        return task;
    }
    _run() {
        dump_1.read()
            .then((data) => (data
            .split(SPLITTER)
            .filter((line) => (line.length))))
            .then((data) => {
            data
                .forEach((str) => {
                const json = JSON.parse(str);
                this._taskBuild(json.desc, json.date);
            });
        });
    }
    _save() {
        dump_1.write(this._tasksToString());
    }
    _tasksToString() {
        return this._tasks
            .map((task) => (JSON.stringify({
            desc: task.desc,
            date: task.date,
        })))
            .join(SPLITTER);
    }
}
exports.default = Journal;
//# sourceMappingURL=index.js.map