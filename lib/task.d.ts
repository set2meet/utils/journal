export declare type TaskCallback<T> = (task: Task<T>) => void;
export declare type TaskDate = string | number;
declare type TaskOptions<T> = {
    desc: T;
    date: TaskDate;
    remove: TaskCallback<T>;
    execute: TaskCallback<T>;
};
export default class Task<Desc> {
    private _desc;
    private _date;
    private _onRemove;
    private _onExecute;
    private _timeout;
    constructor(ops: TaskOptions<Desc>);
    get date(): Date;
    get desc(): Desc;
    remove(): void;
    private _execute;
}
export {};
