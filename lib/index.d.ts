import Task, { TaskDate } from './task';
import { RecursivePartial } from './types';
export declare type JournalTaskCallback<T> = (task: T) => void;
export default class Journal<Desc extends {}> {
    private _tasks;
    private _queueExecTasks;
    private _onTaskExecute;
    constructor();
    onTask(fn: JournalTaskCallback<Desc>): void;
    createTask(desc: Desc, date: TaskDate): Task<Desc>;
    removeTask(desc: RecursivePartial<Desc>): void;
    private _taskBuild;
    private _taskRemoved;
    private _taskExecuted;
    private _run;
    private _save;
    private _tasksToString;
}
