"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.write = exports.read = void 0;
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const fileDumpPath = './journal.dump.log';
const stopWithError = (err) => {
    console.log(`[Journal] Error: ${err.message}`);
    process.exit(1);
};
const hasAccessWriteToFile = (url) => {
    return Promise.resolve()
        .then(() => fs_extra_1.default.ensureFile(url))
        .then(() => fs_extra_1.default.access(url, fs_extra_1.default.constants.W_OK))
        .then(() => (true))
        .catch(() => (false));
};
// dfs = dump fs
// ready - promise to check dum file access
// read  - promise to dumped value,
//  can be resolved after ready only once
// write - promise to write (update) dump file,
//  lock writing by changing 'ready' promise by ne one
const dfs = {};
dfs.read = new Promise((resolve) => {
    dfs.readResolve = resolve;
});
dfs.ready = new Promise((resolve, reject) => {
    Promise
        .resolve()
        .then(() => hasAccessWriteToFile(fileDumpPath))
        .then((access) => {
        if (access) {
            return fs_extra_1.default.readFile(fileDumpPath)
                .then((data) => (dfs.readResolve(data.toString())));
        }
        else {
            throw new Error(`Has no access to dump file ${path_1.default.resolve(fileDumpPath)}`);
        }
    })
        .then(() => {
        resolve();
    })
        .catch((err) => {
        reject();
        stopWithError(err);
    });
});
dfs.write = function (data) {
    dfs.ready.then(() => {
        return dfs.ready = new Promise((resolve) => {
            fs_extra_1.default.writeFile(fileDumpPath, data).then(resolve);
        });
    });
};
exports.read = () => dfs.read;
exports.write = (data) => dfs.write(data);
//# sourceMappingURL=dump.js.map