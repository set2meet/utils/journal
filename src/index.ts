import _filter from 'lodash/filter';
import { read, write } from './dump';
import Task, { TaskDate } from './task';
import { RecursivePartial } from './types';

const SPLITTER = '\n';

export type JournalTaskCallback<T> = (task: T) => void;

export default class Journal<Desc extends {}> {
  private _tasks: Array<Task<Desc>> = [];
  private _queueExecTasks: Array<Task<Desc>> = [];
  private _onTaskExecute: JournalTaskCallback<Desc>;

  constructor() {
    this._run();
  }

  public onTask(fn: JournalTaskCallback<Desc>) {
    this._onTaskExecute = fn;
    this._queueExecTasks.forEach(this._taskExecuted);
    this._queueExecTasks.length = 0;
  }

  public createTask(desc: Desc, date: TaskDate): Task<Desc> {
    const task = this._taskBuild(desc, date);

    this._save();

    return task;
  }

  public removeTask(desc: RecursivePartial<Desc>) {
    _filter<Task<Desc>>(this._tasks, desc).forEach((task) => task.remove());
  }

  private _taskBuild(desc: Desc, date: TaskDate): Task<Desc> {
    const task = new Task<Desc>({
      desc,
      date,
      remove: this._taskRemoved,
      execute: this._taskExecuted,
    });

    this._tasks.push(task);

    return task;
  }

  private _taskRemoved = (task: Task<Desc>) => {
    const inx = this._tasks.indexOf(task);

    if (inx > -1) {
      this._tasks.splice(inx, 1);
      this._save();
    }
  };

  private _taskExecuted = (task: Task<Desc>) => {
    if (this._onTaskExecute) {
      task.remove();
      this._onTaskExecute(task.desc);
    } else {
      this._queueExecTasks.push(task);
    }
  };

  private _run() {
    read()
      .then((data) => (
        data
          .split(SPLITTER)
          .filter((line) => (line.length))
      ))
      .then((data) => {
        data
          .forEach((str) => {
            const json = JSON.parse(str);

            this._taskBuild(json.desc, json.date);
          });
      })
    ;
  }

  private _save() {
    write(this._tasksToString());
  }

  private _tasksToString(): string {
    return this._tasks
      .map((task) => (
        JSON.stringify({
          desc: task.desc,
          date: task.date,
        })
      ))
      .join(SPLITTER)
    ;
  }
}
