export type TaskCallback<T> = (task: Task<T>) => void;

export type TaskDate = string | number;

type TaskOptions<T> = {
  desc: T;
  date: TaskDate;
  remove: TaskCallback<T>;
  execute: TaskCallback<T>;
};

export default class Task<Desc> {
  private _desc: Desc;
  private _date: Date;
  private _onRemove: TaskCallback<Desc>;
  private _onExecute: TaskCallback<Desc>;
  private _timeout: NodeJS.Timeout;

  constructor(ops: TaskOptions<Desc>) {
    const {
      desc,
      date,
      remove,
      execute,
    } = ops;

    this._desc = desc;
    this._onRemove = remove;
    this._onExecute = execute;

    if (typeof date === 'string') {
      // means default date string
      this._date = new Date(date);
    } else
    if (typeof date === 'number') {
      // means ops.date is tiome shift from now
      this._date = new Date(Date.now() + date);
    }

    // schedule task execution event
    this._timeout = setTimeout(() => this._execute(), this._date.getTime() - Date.now());
  }

  public get date(): Date {
    return this._date;
  }

  public get desc(): Desc {
    return this._desc;
  }

  public remove() {
    clearTimeout(this._timeout);
    this._onRemove(this);
  }

  private _execute() {
    this._onExecute(this);
  }
}