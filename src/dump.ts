import path from 'path';
import fs from 'fs-extra';

const fileDumpPath = './journal.dump.log';

const stopWithError = (err: Error) => {
  console.log(`[Journal] Error: ${err.message}`);
  process.exit(1);
};

const hasAccessWriteToFile = (url: string): Promise<boolean> => {
  return Promise.resolve()
    .then(() => fs.ensureFile(url))
    .then(() => fs.access(url, fs.constants.W_OK))
    .then(() => (true))
    .catch(() => (false))
  ;
};

// dfs = dump fs
// ready - promise to check dum file access
// read  - promise to dumped value,
//  can be resolved after ready only once
// write - promise to write (update) dump file,
//  lock writing by changing 'ready' promise by ne one

const dfs: any = {};

dfs.read = new Promise((resolve) => {
  dfs.readResolve = resolve;
});

dfs.ready = new Promise((resolve, reject) => {
  Promise
    .resolve()
    .then(() => hasAccessWriteToFile(fileDumpPath))
    .then((access) => {
      if (access) {
        return fs.readFile(fileDumpPath)
          .then((data) => (
            dfs.readResolve(data.toString())
          ))
        ;
      } else {
        throw new Error(`Has no access to dump file ${path.resolve(fileDumpPath)}`);
      }
    })
    .then(() => {
      resolve();
    })
    .catch((err) => {
      reject();
      stopWithError(err);
    })
  ;
});

dfs.write = function (data: string): void {
  dfs.ready.then(() => {
    return dfs.ready = new Promise((resolve) => {
      fs.writeFile(fileDumpPath, data).then(resolve);
    });
  });
};

export const read = (): Promise<string> => dfs.read;

export const write = (data: string) => dfs.write(data);
